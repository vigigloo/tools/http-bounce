interface Redirection {
  httpCode: number;
  source: URL;
  destination: string;
}

const filePath = Deno.args[0];
const fileContent = await Deno.readTextFile(filePath);
const redirections = new Map<string, Redirection>();

for (const [i, line] of fileContent.split("\n").entries()) {
  const [rawHttpCode, sourceUrl, destination] = line.split(/\s+/);
  if (!rawHttpCode || !sourceUrl) {
    throw Error(`malformed instruction at line ${i+1}`);
  }
  const httpCode = parseInt(rawHttpCode);
  if (httpCode < 400 && httpCode >= 300 && !destination) {
    throw Error(`destination is required at line ${i+1}`);
  }    
  const source = new URL(sourceUrl);
  const index = `${source.host}${source.pathname}`;
  redirections.set(index, {
    httpCode,
    source,
    destination,
  });
}

const ac = new AbortController();

if (Deno.args.length !== 1) {
  throw Error(
    "Usage: deno run --allow-read --allow-net --allow-env index.ts <path_to_instructions_file>",
  );
}
Deno.serve({
  port: Deno.env.get("PORT") ?? 8080,
  hostname: Deno.env.get("HOST") ?? "127.0.0.1",
  handler: function handleRequest(request: Request): Response {
    const host = request.headers.get("Host");
    const url = new URL(request.url);
    const redirection = redirections.get(`${host}${url.pathname}`);
    if (!redirection) {
      return new Response("Not Found", { status: 404 });
    }
    const httpCode = redirection.httpCode;
    if (httpCode < 400 && httpCode >= 300) {
      return Response.redirect(redirection.destination, httpCode);
    } else {
      const headers = {};
      if (redirection.destination) {
        headers['Location'] = redirection.destination;
      }
      return new Response("", {
        status: httpCode,
        headers,
      });
    }
  },
  signal: ac.signal,
  onListen({ port, hostname }: { port: number; hostname: string }) {
    console.log(`Server started at http://${hostname}:${port}`);
  },
});
